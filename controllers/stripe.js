const Product = require('../models/product');
const Cart = require('../models/cart');
const User = require('../models/user');
const Coupon = require('../models/coupon');
const stripe = require('stripe')(process.env.STRIPE_SECRET);

exports.createPaymentIntent = async (req, res) => {

    const user = await User.findOne({ email: res.user.email }).exec();
    const { cartTotal, totalAfterDiscount } = await Cart.findOne({ orderedBy: user._id }).exec();
    
    let finalAmount = 0;
    if (totalAfterDiscount < cartTotal) {
        finalAmount = (totalAfterDiscount).toFixed(2);
    } else {
        finalAmount = (cartTotal).toFixed(2);
    };

    const paymentIntent = await stripe.paymentIntents.create({
        amount: finalAmount * 100,
        currency: 'usd'
    });
    res.send({
        clientSecret: paymentIntent.client_secret,
        cartTotal,
        totalAfterDiscount,
        payable: finalAmount
    })
};