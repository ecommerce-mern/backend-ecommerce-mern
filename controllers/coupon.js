const Coupon = require('../models/coupon');

exports.create = async (req, res) => {
    try {
        const { name, expiry, discount, isPercentage } = req.body;
        if (isPercentage) {
            res.json(await new Coupon({name, expiry, discount, isPercentage}).save());
            return;
        };
        res.json(await new Coupon({name, expiry, discount}).save());
    } catch (error) {
        console.log(error);
    }
};

exports.list = async (req, res) => {
    try {
        const coupons = await Coupon.find({}).sort({ createdAt: -1 }).exec();
        res.json(coupons);
    } catch (error) {
        console.log(error);
    }
};

exports.remove = async (req, res) => {
    try {
        res.json(await Coupon.findByIdAndDelete(req.params.couponId).exec());
    } catch (error) {
        console.log(error);
    }
};

