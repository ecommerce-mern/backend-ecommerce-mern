const Category = require('../models/category');
const Sub = require('../models/sub');
const Product = require('../models/product');
const slugify = require('slugify');

exports.create = async (req, res) => {
    try {
        const { name } = req.body;
        const category = await new Category({ name, slug: slugify(name).toLowerCase() }).save();
        res.json(category);
    } catch (error) {
        res.status(400).send('Create category failed');
    }
};

exports.list = async (req, res) => {
    res.json(await Category.find({}).sort({ createdAt: -1 }).exec());
};

exports.read = async (req, res) => {
    const category = await Category.findOne({ slug: req.params.slug }).exec();
    const products = await Product.find({ category })
        .populate('category')
        .populate('postedBy', '_id name')
        .exec();
    res.json({
        category,
        products
    });
    //res.json(category);
};

exports.update = async (req, res) => {
    try {
        const { name, parent } = req.body;
        const updated = await Category.findOneAndUpdate(
            { slug: req.params.slug}, 
            { name, slug: slugify(name) }, 
            { new: true });
        res.json(updated);
    } catch (error) {
        res.status(400).send('Category update failed');
    }
};

exports.remove = async (req, res) => {
    try {
        const deleted = await Category.findOneAndDelete({ slug: req.params.slug }).exec();
        res.json(deleted);
    } catch (error) {
        res.status(400).send('Category removal failed');
    }
};

exports.getSubs = (req, res) => {
    Sub.find({parent: req.params._id}).exec((err, subs) => {
        if (err) console.log(err);
        res.json(subs);
    })
};