const User = require('../models/user');
const Product = require('../models/product');
const Cart = require('../models/cart');
const Coupon = require('../models/coupon');
const Order = require('../models/order');

exports.userCart = async (req, res) => {
    const { cart } = req.body;

    let products = [];
    let cartTotal = 0;
    const user = await User.findOne({ email: res.user.email }).exec();
    let cartExistsByThisUser = await Cart.findOne({ orderedBy: user._id}).exec();
    
    if (cartExistsByThisUser) {
        cartExistsByThisUser.remove();
    };

    for (let product = 0; product < cart.length; product++) {
        let object = cart[product];
        object.product = cart[product]._id;
        object.count = cart[product].count;
        object.color = cart[product].color;
        let { price } = await Product.findById(cart[product]._id).select('price').exec();
        object.price = price;
        products.push(object);

        cartTotal += object.count * object.price;
    };

    let newCart = await new Cart({
        products,
        cartTotal,
        orderedBy: user._id,
    }).save();

    res.json({ ok: true });
};

exports.getUserCart = async (req, res) => {
    const user = await User.findOne({ email: res.user.email }).exec();

    let cart = await Cart.
        findOne({ orderedBy: user._id })
        .populate('products.product', '_id title price')
        .populate('products', 'totalAfterDiscount cartTotal')
        .exec();

    const { products, cartTotal, totalAfterDiscount } = cart;
    res.json({ products, cartTotal, totalAfterDiscount });
};

exports.emptyCart = async (req, res) => {
    const user = await User.findOne({ email: res.user.email }).exec();
    const cart = await Cart.findOneAndRemove({ orderedBy: user._id }).exec();
    res.json(cart);
};

exports.saveAddress = async (req, res) => {
    const user = await User.findOneAndUpdate({ email: res.user.email }, { address: req.body.address }).exec();
    res.json({ ok: true });
};

exports.applyCouponToUserCart = async (req, res) => {
    const { coupon } = req.body;
    const validCoupon = await Coupon.findOne({ name: coupon }).exec();
    
    if (validCoupon === null) {
        return res.json({
            err: 'Invalid coupon'
        });
    };

    const user = await User.findOne({ email: res.user.email }).exec();
    let { products, cartTotal } = await Cart.findOne({ orderedBy: user._id })
        .populate('products.product', '_id title price')
        .exec();

    let totalAfterDiscount = cartTotal;
    if (validCoupon.isPercentage) {
        totalAfterDiscount = (cartTotal * (1 - (validCoupon.discount / 100))).toFixed(2);
    } else {
        totalAfterDiscount = cartTotal - validCoupon.discount;
    };
    await Cart.findOneAndUpdate({ orderedBy: user._id }, {totalAfterDiscount}, {new: true}).exec();
    res.json(totalAfterDiscount);
};

exports.createOrder = async (req, res) => {
    const { paymentIntent } = req.body.stripeResponse;
    const user = await User.findOne({ email: res.user.email }).exec();

    let { products } = await Cart.findOne({ orderedBy: user._id }).exec();
    let newOrder = await new Order({
        products,
        paymentIntent,
        orderedBy: user._id
    }).save();

    let bulkOption = products.map((item) => {
        return {
            updateOne: {
                filter: {_id: item.product._id},
                update: {$inc: {quantity: -item.count, sold: +item.count}}
            }
        };
    });
    let updated = await Product.bulkWrite(bulkOption, {});

    res.json({ ok: true });
};

exports.orders = async (req, res) => {
    let user = await User.findOne({ email: res.user.email }).exec();
    let userOrders = await Order.find({ orderedBy: user._id })
        .populate('products.product')
        .exec();
    
    res.json(userOrders);
};

exports.addToWishlist = async (req, res) => {
    const { productId } = req.body;
    let user = await User.findOneAndUpdate({ email: res.user.email }, {
        $addToSet: { wishlist: productId }
    }, {new: true}).exec();

    res.json({ ok: true });
};

exports.removeFromWishlist = async (req, res) => {
    const { productId } = req.params;
    let user = await User.findOneAndUpdate({ email: res.user.email }, {
        $pull: { wishlist: productId }
    }, {new: true}).exec();

    res.json({ ok: true });
};

exports.wishlist = async (req, res) => {
    const list = await User
        .findOne({ email: res.user.email })
        .select('wishlist')
        .populate('wishlist')
        .exec();
        
    res.json(list);
};
