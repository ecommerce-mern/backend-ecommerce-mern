//imports
const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const cors = require('cors');
const { readdirSync } = require('fs');

//env vars and express app
require('dotenv').config();
const app = express();

//db
mongoose.connect(process.env.DATABASE, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
})
.then(() => console.log('DB CONNECTED'))
.catch(e => console.log(`DB CONNECTION ERROR: ${e}`));

//middleware
app.use(morgan("dev"));
app.use(bodyParser.json({ limit: "2mb" }));
app.use(cors());

//routes middleware
readdirSync('./routes').map( (r) => app.use('/api', require(`./routes/${r.slice(0, -3)}`)) );

//port
const port = process.env.PORT || 8000;

//start server
app.listen(port, () => console.log(`Server is now running on port ${port}`));