# Backend ecommerce MERN

The Express/MongoDB backend for a fully deployable ecommerce website using Stripe

This backend is meant to be used with the corresponding frontend, which can be found [here](https://gitlab.com/ecommerce-mern/frontend-ecommerce-mern)


# Getting started

### Development mode

#### Step 1
* Clone this repository
* Go into the root folder, and run 'npm i'

#### Step 2
* Sign up to MongoDB Atlas, and create a new project
* Sign up to Cloudinary
* Now, create a .env file in the root folder and add the following
~~~
DATABASE='mongodb+srv://name-of-project:<password>@name-of-project.p5g0d.mongodb.net/<dbname>?retryWrites=true&w=majority'
PORT=8000

CLOUDINARY_CLOUD_NAME=xxxxxxxx
CLOUDINARY_API_KEY=xxxxxxxxxxx
CLOUDINARY_API_SECRET=XXXXXXXXXXXXXXXXXXX

STRIPE_SECRET=sk_test_XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
~~~
where the different variables need to be filled in using your secrets from Cloudinary, MongoDB Atlas and Stripe

#### Step 3
* Go to your Firebase account, go to your Project settings, then into Service Accounts
* Press 'Generate new private key' at the bottom, then press 'Generate Key' on the popup
* Save the downloaded file inside /config as 'fbServiceAccountKey.json'

#### Step 4
* Run 'npm start'


# Prerequisites
* Node


# Authors
* Jens Christian Thiemann Hetland