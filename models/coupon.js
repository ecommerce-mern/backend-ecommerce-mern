const mongoose = require('mongoose');
const { ObjectId } = mongoose.Schema;

const couponSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            uppercase: true,
            trim: true,
            unique: true,
            required: 'Name is required',
            minlength: [3, 'Too short'],
            maxlength: [20, 'Too long']
        },
        expiry: {
            type: Date,
            required: true,
        },
        discount: {
            type: Number,
            required: true
        },
        isPercentage: {
            type: Boolean,
            default: false
        }
    }, 
    { timestamps: true }
);

module.exports = mongoose.model('Coupon', couponSchema);